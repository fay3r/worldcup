package com.demo.springboot.rest;


import com.demo.springboot.domain.*;
import com.demo.springboot.service.WordlCupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/match")
public class Controler {
    private static final Logger LOOGGER = LoggerFactory.getLogger(Controler.class);

    @Autowired
    private WordlCupService operacje;
    @Autowired
    private WinnerDto winner;

    @RequestMapping(value = "/show-winner",method = RequestMethod.POST)
    public ResponseEntity<Map<String,WinnerDto>> result(@RequestBody MatchDto matchDto){
        winner=operacje.result(matchDto);
        
        Map <String,WinnerDto> back = new HashMap<>();
        back.put("winner",winner);

        return new ResponseEntity(back,HttpStatus.OK);
    }

}

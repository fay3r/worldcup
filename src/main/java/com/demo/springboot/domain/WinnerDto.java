package com.demo.springboot.domain;


import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Component;


@ToString
@NoArgsConstructor
@Component
public class WinnerDto {
    private String team;
    private String goals;

    public WinnerDto(String Team, String Goals){
        this.team=Team;
        this.goals=Goals;
    }

    public String getTeam() {
        return team;
    }

    public String getGoals() {
        return goals;
    }
}

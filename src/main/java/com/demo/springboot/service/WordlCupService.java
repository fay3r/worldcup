package com.demo.springboot.service;

import com.demo.springboot.domain.*;

public interface WordlCupService {
    WinnerDto result(MatchDto x);
}

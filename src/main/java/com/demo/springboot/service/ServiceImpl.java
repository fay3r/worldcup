package com.demo.springboot.service;

import com.demo.springboot.domain.*;
import org.springframework.stereotype.Service;

@Service
public class ServiceImpl implements WordlCupService {

    @Override
    public WinnerDto result(MatchDto x) {
        String[] dane;
        dane=x.getMatch().split(" " );
        int x1=Integer.parseInt(dane[1]);
        int x2=Integer.parseInt(dane[3]);

        if(x1<x2)
        {
            return new WinnerDto(dane[4],Integer.toString(x2));
        }
        if(x2<x1)
        {
            return new WinnerDto(dane[0],Integer.toString(x1));
        }
        else{
            return null;
        }
    }
}
